package fr.n7.cnam.nfp121.pr01;

import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.n7.cnam.nfp121.pr01.actions.EffacerAction;
import fr.n7.cnam.nfp121.pr01.actions.SupprimerItemAction;
import fr.n7.cnam.nfp121.pr01.actions.TerminerAction;
import fr.n7.cnam.nfp121.pr01.actions.ValiderAction;

public class SaisiesSwing {
	private final String MAIN_WINDOW_TITLE = "Saisie données";
	
	public static final String VALIDATE_BUTTON_ID = "valid_button";
	public static final String DELETE_BUTTON_ID = "delete_button";
	public static final String TERMINATE_BUTTON_ID = "terminate_button";
	public static final String REMOVE_ITEM_BUTTON_ID = "remove_item_button";
	public static final String ABSCISSE_INPUT_TEXT_ID = "abcsisse_input_text";
	public static final String ORDONNEE_INPUT_TEXT_ID = "ordonnee_input_text";
	public static final String VALEUR_INPUT_TEXT_ID = "valeur_input_text";
	public static final String LIST_COMPONENT_ID = "list";
	
	private JFrame window;
	private Map<String, Component> componentsRefMap;
	private ArrayList<AData> aDataList;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SaisiesSwing app = new SaisiesSwing();
					app.window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public SaisiesSwing() {
		this.componentsRefMap = new HashMap<String, Component>();
		this.aDataList = new ArrayList<AData>();
		this.window = new JFrame(MAIN_WINDOW_TITLE);
		window.setBounds(100, 100, 450, 200);
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container content = this.window.getContentPane();
		
		this.initContentPane(content);
	}
	
	public void save() {
		JTextField abscisseField = (JTextField) this.componentsRefMap.get(SaisiesSwing.ABSCISSE_INPUT_TEXT_ID);
		String abscisseString = abscisseField.getText();
		JTextField ordonneeField = (JTextField) this.componentsRefMap.get(SaisiesSwing.ORDONNEE_INPUT_TEXT_ID);
		String ordonneeString = ordonneeField.getText();
		JTextField valeurField = (JTextField) this.componentsRefMap.get(SaisiesSwing.VALEUR_INPUT_TEXT_ID);
		String valeurString = valeurField.getText();
		
		Position position = new Position(Integer.parseInt(abscisseString), Integer.parseInt(ordonneeString));
		this.aDataList.add(new AData(position, Double.parseDouble(valeurString)));
		
		this.updateList();
	}
	
	public void removeItem(int indexToRemove) {
		this.aDataList.remove(indexToRemove);
		this.updateList();
	}
	
	public Frame getFrame() {
		return this.window;
	}
	
	public void exit() {
		this.window.dispose();
	}
	
	public ArrayList<AData> getDataList() {
		ArrayList<AData> dataList = new ArrayList<>();
		dataList.addAll(this.aDataList);
		return dataList;
	}
	
	private void updateList() {
		List list = (List) this.componentsRefMap.get(LIST_COMPONENT_ID);
		list.removeAll();
		for (AData data : this.aDataList) {
			list.add(data.toString());
		}
	}
	
	private void initContentPane(Container content) {
		GridLayout mainLayout = new GridLayout(3, 1, 0, 0);
		content.setLayout(mainLayout);
		content.add(this.createDataInputContainer());
		content.add(this.createButtonsContainer());
		content.add(this.createListComponent());
	}
	
	private Container createDataInputContainer() {
		Container container = new Container();
		container.setLayout(new GridLayout(1, 3, 10, 10));
		
		container.add(this.createInputContainer("Abscisse", ABSCISSE_INPUT_TEXT_ID));
		container.add(this.createInputContainer("Ordonnée", ORDONNEE_INPUT_TEXT_ID));
		container.add(this.createInputContainer("Valeur", VALEUR_INPUT_TEXT_ID));
		
		return container;
	}
	
	private Container createButtonsContainer() {
		FlowLayout layout = new FlowLayout();
		Container container = new Container();
		container.setLayout(layout);
		
		JButton validerButton = new JButton("Valider");
		validerButton.addActionListener(new ValiderAction(this.componentsRefMap, this));
		this.componentsRefMap.put(VALIDATE_BUTTON_ID, validerButton);
		
		JButton effacerButton = new JButton("Effacer");
		effacerButton.addActionListener(new EffacerAction(this.componentsRefMap));
		this.componentsRefMap.put(DELETE_BUTTON_ID, effacerButton);
		
		JButton removeItemButton = new JButton("Supp. donnée selectionnée");
		removeItemButton.addActionListener(new SupprimerItemAction(this.componentsRefMap, this));
		this.componentsRefMap.put(REMOVE_ITEM_BUTTON_ID, removeItemButton);
		
		JButton terminerButton = new JButton("Terminer");
		terminerButton.addActionListener(new TerminerAction(this));
		this.componentsRefMap.put(TERMINATE_BUTTON_ID, terminerButton);
		
		container.add(validerButton);
		container.add(effacerButton);
		container.add(removeItemButton);
		container.add(terminerButton);
		
		return container;
	}
	
	private Container createInputContainer(String labelText, String inputTextId) {
		GridLayout layout = new GridLayout(2, 1, 0, 0);
		Container container = new Container();
		container.setLayout(layout);
		
		JLabel label = new JLabel(labelText, JLabel.CENTER);
		JTextField inputText = new JTextField();
		
		inputText.getDocument().addDocumentListener(new DocumentListener() {
			private void updateBackgroundColor(DocumentEvent e) {
				inputText.setBackground(null);
			}
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				this.updateBackgroundColor(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				this.updateBackgroundColor(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				this.updateBackgroundColor(e);
			}
		});
		container.add(label);
		container.add(inputText);
		
		this.componentsRefMap.put(inputTextId, inputText);
		
		return container;
	}
	
	private Component createListComponent() {
		List list = new List();
		this.componentsRefMap.put(LIST_COMPONENT_ID, list);
		return list;
	}
	
}
