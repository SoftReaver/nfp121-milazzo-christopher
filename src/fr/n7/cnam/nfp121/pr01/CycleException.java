package fr.n7.cnam.nfp121.pr01;

public class CycleException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public CycleException(String message) {
		super(message);
	}
	
	public CycleException() {
		super();
	}
}
