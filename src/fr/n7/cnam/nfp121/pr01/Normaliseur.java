package fr.n7.cnam.nfp121.pr01;

/**
  * Normaliseur normalise les données d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Normaliseur extends Traitement {
	private double debut;
	private double fin;
	private Max max;
	private Min min;

	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		this.max = new Max();
		this.min = new Min();
		super.gererDebutLotLocal(nomLot);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.max.traiter(position, valeur);
		this.min.traiter(position, valeur);
		
		double max = this.max.getMax();
		double min = this.min.getMin();
		double a = (max - min) / (this.fin - this.debut);
		double b = this.debut - (a * min);
		
		valeur = (a * valeur) + b;
		
		super.traiter(position, valeur);
	}

	@Override
	protected String toStringComplement() {
		return this.debut + ", " + this.fin;
	}
}
