package fr.n7.cnam.nfp121.pr01;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées)
 * lors du traitement de ce lot.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {
	private Map<String, ArrayList<Position>> lotsMap;
	
	public Maj() {
		this.lotsMap = new HashMap<String, ArrayList<Position>>();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
	}

	public ArrayList<Position> getPositionsForLot(String lotName) {
		return this.lotsMap.get(lotName);
	}
}
