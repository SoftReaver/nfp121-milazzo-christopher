package fr.n7.cnam.nfp121.pr01;

public class Min extends Traitement {
	private Double min;
	
	public Min() {
		this.min = null;
	}
	
	public double getMin() {
		return this.min.doubleValue();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if (min == null || valeur < min) {
			min = valeur;
		}
		super.traiter(position, valeur);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": min = " + this.min);
	}
}
