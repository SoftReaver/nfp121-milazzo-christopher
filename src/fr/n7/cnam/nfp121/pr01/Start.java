package fr.n7.cnam.nfp121.pr01;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import fr.n7.cnam.nfp121.pr01.io.DataReader;
import fr.n7.cnam.nfp121.pr01.io.DataReaderException;

public class Start {
	public static final String ARG_TREATMENT_DESC_FILE = "traitdesc";
	
	public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, FileNotFoundException, InstantiationException {
		HashMap<String, String> argumentsMap = fetchArguments(args);
		String treatmentDescFileString = argumentsMap.get(ARG_TREATMENT_DESC_FILE);
		Scanner in;
		if (treatmentDescFileString == null) {
			in = new Scanner(getDefaultTreatment());
		} else {
			in = new Scanner(new File(treatmentDescFileString));
		}
		
		HashMap<String, Traitement> env = new HashMap<>();
		Traitement traitement = new TraitementBuilder().traitement(in, env);
		Analyseur analyseur = new Analyseur(traitement);
		in.close();
		
		for (String filePath : fetchFilesFromArgs(args)) {
			try {				
				DataReader dataReader = DataReader.getReaderForFile(filePath);
				analyseur.traiter(dataReader.fetchDataFromFile(filePath), filePath);
			} catch (DataReaderException e) {
				System.err.println("!!! Impossible d'analyser le fichier " + filePath + ". Vérifier qu'il respecte le format attendu!");
			}
		}
	}
	
	private static ArrayList<String> fetchFilesFromArgs(String[] arg) {
		ArrayList<String> fileList = new ArrayList<String>();

		Scanner sc = new Scanner(splitArgsToString(arg));
		while (sc.hasNext()) {
			String value = sc.next();
			if (value.startsWith("-")) {
				sc.next();
			} else {				
				fileList.add(value);
			}
		}
		sc.close();
		return fileList;
	}
	
	private static HashMap<String, String> fetchArguments(String[] arg) {
		HashMap<String, String> argumentsMap = new HashMap<String, String>();
		
		Scanner sc = new Scanner(splitArgsToString(arg));
		while (sc.hasNext()) {
			String value = sc.next();
			if (value.startsWith("-")) {
				String paramValue = sc.next();
				argumentsMap.put(value.substring(1), paramValue);
			}
		}
		sc.close();
		return argumentsMap;
	}
	
	private static String splitArgsToString(String[] args) {
		String argumentsString = "";
		for (int i = 0; i < args.length; i++) {
			argumentsString += args[i] + " ";
		}
		return argumentsString;
	}
	
	private static String getDefaultTreatment() {
		String calculs = "id X-p positions 0 1 id X-M max 0 1 id X-s somme 0 1 id X-sp sommeParPosition 0";
		String generateur = "generateurXML 1 java.lang.String ./produits/NOM--genere.xml";
		String traitementString = generateur.replaceAll("NOM", "brut") + " 3"
			+ " " + calculs.replaceAll("X-", "brut-") + " 0"
			+ " " + "id spp supprimerPlusPetit 1 double 0.0 1 id spg supprimerPlusGrand 1 double 10.0 2"
				+ " " + generateur.replaceAll("NOM", "valides") + " 0"
				+ " " + calculs.replaceAll("X-", "valides-") + " 0"
			+ " " + "id n normaliseur 2 double 0.0 double 100.0 2"
				+ " " + generateur.replaceAll("NOM", "normalisees") + " 0"
				+ " " + calculs.replaceAll("X-", "normalisees-") + " 0";
		
		return traitementString;
	}
}
