package fr.n7.cnam.nfp121.pr01;

import java.util.*;

/**
  * SommeParPosition 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {
	private Map<Position, Double> counter;
	
	public SommeParPosition() {
		this.counter = new HashMap<Position, Double>();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		Double positionCounter = this.counter.get(position);
		if (positionCounter == null) {
			positionCounter = 0.0;
		}
		positionCounter += valeur;
		this.counter.put(position, positionCounter);
		
		super.traiter(position, valeur);
	}
	
	public Double getTotalForPosition(Position position) {
		Double total = this.counter.get(position);
		return (total == null) ? 0.0 : total;
	}
}
