package fr.n7.cnam.nfp121.pr01;

public class AData {
	public Position position;
	public Double valeur;
	
	public AData(Position position, Double valeur) {
		this.position = position;
		this.valeur = valeur;
	}
	
	@Override
	public String toString() {
		return "@(" + position.x + ", " + position.y + ") = " + valeur;
	}
}
