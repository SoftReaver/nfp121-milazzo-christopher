package fr.n7.cnam.nfp121.pr01;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * GenerateurXML écrit dans un fichier, à charque fin de lot, toutes
 * les données lues en indiquant le lot dans le fichier XML.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Traitement {
	Element rootElement;
	Element lotsElement;
	Element lotElement;
	Document xlmDocument;
	XMLOutputter xlmWritter;
	String fileName;

	public GenerateurXML(String nomFichier) {
		super();
		this.rootElement = new Element("resultats");
		this.lotsElement = new Element("lots");
		this.rootElement.addContent(lotsElement);
		this.xlmDocument = new Document(rootElement, new DocType("resultats", "fichiers/generateur.dtd"));
		this.xlmWritter = new XMLOutputter(Format.getPrettyFormat());
		this.fileName = nomFichier;
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		super.gererDebutLotLocal(nomLot);
		this.lotElement = new Element("lot");
		this.lotElement.setAttribute("nom", nomLot);
		this.lotsElement.addContent(lotElement);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.addDataToDocument(position, valeur);		
		super.traiter(position, valeur);
	}

	@Override
	protected void gererFinLotLocal(String nomLot) {
		super.gererFinLotLocal(nomLot);
		try {
			FileOutputStream out = new FileOutputStream(this.fileName);
			this.xlmWritter.output(this.xlmDocument, out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void addDataToDocument(Position position, double valeur) {
		Element donneeElement = new Element("donnee");
		Element xElement = new Element("x");
		Element yElement = new Element("y");
		
		xElement.setText(String.valueOf(position.x));
		yElement.setText(String.valueOf(position.y));
		
		donneeElement.setAttribute("valeur", String.valueOf(valeur));
		donneeElement.addContent(xElement);
		donneeElement.addContent(yElement);
		
		this.lotElement.addContent(donneeElement);
	}
		
}
