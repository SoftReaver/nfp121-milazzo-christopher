package fr.n7.cnam.nfp121.pr01;

import java.util.ArrayList;

public class Unique extends Traitement {
	ArrayList<Position> knownPositions;
	
	public Unique() {
		this.knownPositions = new ArrayList<Position>();
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		this.knownPositions = new ArrayList<Position>();
		super.gererDebutLotLocal(nomLot);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if (!this.knownPositions.contains(position)) {
			this.knownPositions.add(position);
			super.traiter(position, valeur);
		}
	}
}
