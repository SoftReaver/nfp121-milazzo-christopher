package fr.n7.cnam.nfp121.pr01.io;

public class DataReaderException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public DataReaderException() {
		super();
	}
	
	public DataReaderException(String message) {
		super(message);
	}
}
