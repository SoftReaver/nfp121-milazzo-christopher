package fr.n7.cnam.nfp121.pr01.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import fr.n7.cnam.nfp121.pr01.AData;

public class DataWriterTxt implements DataWriter {

	@Override
	public void writeData(ArrayList<AData> data, String filePath) {
		try {
			if (data == null || data.size() == 0) {
				throw new DataWriterException("Aucune donnée à écrire");
			}
			FileWriter fileWriter = new FileWriter(new File(filePath));
			for (AData oneData : data) {
				int x = oneData.position.x;
				int y = oneData.position.y;
				double value = oneData.valeur;
				String line = x + " " + y + " 1 " + value + "\n";
				fileWriter.write(line);
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new DataWriterException(e.getMessage());
		}
	}

}
