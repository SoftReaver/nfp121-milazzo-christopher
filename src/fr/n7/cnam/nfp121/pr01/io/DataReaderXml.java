package fr.n7.cnam.nfp121.pr01.io;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import fr.n7.cnam.nfp121.pr01.Position;

public class DataReaderXml implements DataReader {

	@Override
	public ArrayList<SimpleImmutableEntry<Position, Double>> fetchDataFromFile(String filePath) throws DataReaderException {
		ArrayList<SimpleImmutableEntry<Position, Double>> source = new ArrayList<AbstractMap.SimpleImmutableEntry<Position,Double>>();

		SAXBuilder domBuilder = new SAXBuilder(XMLReaders.DTDVALIDATING);
		Document document;
		try {
			document = domBuilder.build(new File(filePath));
			
			Element racine = document.getRootElement();
			
			for (Element donneeElement : racine.getChildren()) {
				Double valeur = donneeElement.getAttribute("valeur").getDoubleValue();
				int x = Integer.parseInt(donneeElement.getChild("x").getText());
				int y = Integer.parseInt(donneeElement.getChild("y").getText());
				Position position = new Position(x, y);	
				
				source.add(new SimpleImmutableEntry<Position, Double>(position, valeur));
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new DataReaderException();
		}
		
		return source;
	}

}
