package fr.n7.cnam.nfp121.pr01.io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import fr.n7.cnam.nfp121.pr01.AData;

public class DataWriterXml implements DataWriter {

	@Override
	public void writeData(ArrayList<AData> data, String filePath) {
		try {
			if (data == null || data.size() == 0) {
				throw new DataWriterException("Aucune donnée à écrire");
			}
			Element rootElement = new Element("donnees");
			Document xlmDocument = new Document(rootElement, new DocType("donnees", "./donnees1.dtd"));
			XMLOutputter xlmWritter = new XMLOutputter(Format.getPrettyFormat());
			int index = 1;
			for (AData oneData : data) {
				int x = oneData.position.x;
				int y = oneData.position.y;
				double value = oneData.valeur;
				
				Element xElement = new Element("x");
				xElement.addContent(String.valueOf(x));
				Element yElement = new Element("y");
				yElement.addContent(String.valueOf(y));
				
				Element dataElement = new Element("donnee");
				dataElement.setAttribute("id", String.valueOf(index));
				dataElement.setAttribute("valeur", String.valueOf(value));
				dataElement.addContent(xElement);
				dataElement.addContent(yElement);
				
				rootElement.addContent(dataElement);
				index++;
			}
			
			FileOutputStream out = new FileOutputStream(filePath);
			xlmWritter.output(xlmDocument, out);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new DataWriterException(e.getMessage());
		}
	}

}
