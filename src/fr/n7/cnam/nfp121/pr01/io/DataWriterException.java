package fr.n7.cnam.nfp121.pr01.io;

public class DataWriterException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public DataWriterException() {
		super();
	}
	
	public DataWriterException(String message) {
		super(message);
	}
}
