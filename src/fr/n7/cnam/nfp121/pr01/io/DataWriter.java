package fr.n7.cnam.nfp121.pr01.io;

import java.util.ArrayList;

import fr.n7.cnam.nfp121.pr01.AData;

public interface DataWriter {
	/**
	 * Write the given data into the given file.
	 * 
	 * @param data
	 * @param filePath
	 */
	public void writeData(ArrayList<AData> data, String filePath) throws DataWriterException;
}
