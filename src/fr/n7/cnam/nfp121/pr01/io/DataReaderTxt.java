package fr.n7.cnam.nfp121.pr01.io;

import java.util.AbstractMap.SimpleImmutableEntry;

import fr.n7.cnam.nfp121.pr01.Position;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.AbstractMap;
import java.util.ArrayList;

public class DataReaderTxt implements DataReader {

	@Override
	public ArrayList<SimpleImmutableEntry<Position, Double>> fetchDataFromFile(String filePath) {
		ArrayList<SimpleImmutableEntry<Position, Double>> source = new ArrayList<AbstractMap.SimpleImmutableEntry<Position,Double>>();
		
		try (BufferedReader in = new BufferedReader(new FileReader(filePath))) {
			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 4;	// 4 mots sur chaque ligne
				double valeur = Double.parseDouble(mots[3]);
				int x = Integer.parseInt(mots[0]);
				int y = Integer.parseInt(mots[1]);
				Position position = new Position(x, y);
				source.add(new SimpleImmutableEntry<Position, Double>(position, valeur));
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new DataReaderException();
		}
		
		return source;
	}

}
