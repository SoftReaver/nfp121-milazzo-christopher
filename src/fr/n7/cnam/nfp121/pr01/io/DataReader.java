package fr.n7.cnam.nfp121.pr01.io;

import java.util.ArrayList;
import java.util.AbstractMap.SimpleImmutableEntry;

import fr.n7.cnam.nfp121.pr01.Position;

public interface DataReader {
	/**
	 * Read data from the given file and return it. A data is a position and an associated value (double)
	 * if no data were read then an empty arrayList should be returned
	 * 
	 * @param filePath
	 * @return A list of data.
	 */
	public ArrayList<SimpleImmutableEntry<Position, Double>> fetchDataFromFile(String filePath) throws DataReaderException;
	
	/**
	 * Try to figure out wich dataReader to use, using the file extention, and returns it.
	 * May throw DataReaderException if dataReader could not be figured out.
	 * 
	 * @param filePath
	 *  
	 */
	public static DataReader getReaderForFile(String filePath) throws DataReaderException {
		int index = filePath.lastIndexOf(".");
		if (index == -1) {			
			throw new DataReaderException("The file " + filePath + " has no extention. It is impossible for the DataReader to guess file format type.");
		}
		
		String fileExtention = filePath.substring(index);
		DataReader dataReader;
		
		switch (fileExtention) {
		case ".xml":
			dataReader = new DataReaderXml();
			break;
		case ".txt":
			dataReader = new DataReaderTxt();
			break;
		default:
			throw new DataReaderException("Unknown extention file '" + fileExtention + "'. Please manually specify the datareader to use.");
		}
		return dataReader;
	}
}
