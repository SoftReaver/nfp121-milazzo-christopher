package fr.n7.cnam.nfp121.pr01;

import java.util.HashMap;
import java.util.Map;

/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {
	private Map<Position, Double> dataMap;
	
	public Donnees() {
		this.dataMap = new HashMap<>();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.dataMap.put(position, valeur);
		super.traiter(position, valeur);
	}

	public int nombre() {
		return this.dataMap.size();
	}

	public Double valeur(Position position) {
		return dataMap.get(position);
	}

}
