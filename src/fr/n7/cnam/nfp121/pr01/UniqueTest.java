package fr.n7.cnam.nfp121.pr01;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UniqueTest extends TraitementTestAbstrait {
	private Unique unique;
	private Traitement unTraitement;

	@Override
	protected Unique nouveauTraitement() {
		return new FabriqueTraitementConcrete().unique();
	}

	@Override
	public void setUp() {
		super.setUp();
		this.unique = nouveauTraitement();
		this.unTraitement = new Traitement() {
			@Override
			public void traiter(Position position, double valeur) {
				throw new SpecialException();
			}
		};
	}

	@Test
	public void testerChainage() {
		testerChainage(true, true);
	}

	@Test
	public void testerUniqueNominal() {
		boolean check = true;
		this.unique.ajouterSuivants(this.unTraitement);
		this.unique.gererDebutLot("Lot1");
		
		try {
			this.unique.traiter(new Position(1, 2), 10.0);
		} catch (SpecialException NOP) {
		}
		
		try {
			this.unique.traiter(new Position(1, 2), 5.5);
			this.unique.gererFinLot("Lot1");
		} catch (SpecialException e) {
			check = false;
		} finally {
			assertTrue("Unique ne traite la donnée de la position (1, 2) qu'une seule fois.", check);
		}
	}
	
	protected class SpecialException extends RuntimeException {
		private static final long serialVersionUID = 1L;
	}
}
