package fr.n7.cnam.nfp121.pr01.actions;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import fr.n7.cnam.nfp121.pr01.SaisiesSwing;
import fr.n7.cnam.nfp121.pr01.io.DataWriterException;
import fr.n7.cnam.nfp121.pr01.io.DataWriterTxt;
import fr.n7.cnam.nfp121.pr01.io.DataWriterXml;

public class TerminerAction implements ActionListener {
	private SaisiesSwing context;
	private JDialog frame;
	private JFileChooser fileChooser;
	
	public TerminerAction(SaisiesSwing context) {
		this.context = context;
		this.fileChooser = new JFileChooser();
		this.fileChooser.setDialogTitle("Enregistrer sous");
		this.fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		showWindow();
	}
	
	private void showWindow() {
		this.frame = new JDialog(context.getFrame(), "Enregistrer", true);
		FlowLayout layout = new FlowLayout();
		Container container = new Container();
		container.setLayout(layout);
		
		JButton saveTxtButton = new JButton("Enregistrer au format TXT");
		saveTxtButton.addActionListener(new SaveTxtAction(context, this.fileChooser));

		JButton saveXmlButton = new JButton("Enregistrer au format XML");
		saveXmlButton.addActionListener(new SaveXmlAction(context, this.fileChooser));
		
		JButton cancelButton = new JButton("Retour");
		cancelButton.addActionListener(new CloseAction(frame));
		
		container.add(saveTxtButton);
		container.add(saveXmlButton);
		container.add(cancelButton);
		
		frame.add(container);
		frame.setBounds(120, 120, 200, 150);
		frame.setResizable(false);
		frame.setVisible(true);
	}
	
	class SaveXmlAction implements ActionListener {
		private SaisiesSwing context;
		private JFileChooser fileChooser;
		
		public SaveXmlAction(SaisiesSwing context, JFileChooser fileChooser) {
			this.context = context;
			this.fileChooser = fileChooser;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				fileChooser.resetChoosableFileFilters();
				fileChooser.setFileFilter(new FileNameExtensionFilter("*.xml", "xml"));
				int userSelection = fileChooser.showSaveDialog(null);
				 
				if (userSelection == JFileChooser.APPROVE_OPTION) {
				    String filePath = fileChooser.getSelectedFile().getAbsolutePath();
				    new DataWriterXml().writeData(context.getDataList(), filePath);
				    context.exit();
				}
			} catch (DataWriterException error) {
				error.printStackTrace();
				JOptionPane.showMessageDialog(null, "Une erreur s'est produite lors de la tentative d'enregistrement des données. Voir les logs pour plus de détails.", "Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	class SaveTxtAction implements ActionListener {
		private SaisiesSwing context;
		private JFileChooser fileChooser;
		
		public SaveTxtAction(SaisiesSwing context, JFileChooser fileChooser) {
			this.context = context;
			this.fileChooser = fileChooser;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				fileChooser.resetChoosableFileFilters();
				fileChooser.setFileFilter(new FileNameExtensionFilter("*.txt", "txt"));
				int userSelection = fileChooser.showSaveDialog(null);
				 
				if (userSelection == JFileChooser.APPROVE_OPTION) {
				    String filePath = fileChooser.getSelectedFile().getAbsolutePath();
				    new DataWriterTxt().writeData(context.getDataList(), filePath);
				    context.exit();
				}
			} catch (DataWriterException error) {
				error.printStackTrace();
				JOptionPane.showMessageDialog(null, "Une erreur s'est produite lors de la tentative d'enregistrement des données. Voir les logs pour plus de détails.", "Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	class CloseAction implements ActionListener {
		private JDialog frame;
		
		public CloseAction(JDialog frame) {
			this.frame = frame;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			this.frame.dispose();
		}
	}
}
