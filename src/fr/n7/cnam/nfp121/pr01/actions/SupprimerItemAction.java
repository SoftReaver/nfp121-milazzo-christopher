package fr.n7.cnam.nfp121.pr01.actions;

import java.awt.Component;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import fr.n7.cnam.nfp121.pr01.SaisiesSwing;

public class SupprimerItemAction implements ActionListener {
	private Map<String, Component> componentsRefMap;
	private SaisiesSwing context;
	
	public SupprimerItemAction(Map<String, Component> componentsRefMap, SaisiesSwing context) {
		this.componentsRefMap = componentsRefMap;
		this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		List list = (List) this.componentsRefMap.get(SaisiesSwing.LIST_COMPONENT_ID);
		int indexToRemove = list.getSelectedIndex();
		
		if (indexToRemove >= 0) {
			context.removeItem(indexToRemove);
		}
	}

}
