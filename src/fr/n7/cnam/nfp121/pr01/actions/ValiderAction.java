package fr.n7.cnam.nfp121.pr01.actions;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JTextField;

import fr.n7.cnam.nfp121.pr01.SaisiesSwing;

public class ValiderAction implements ActionListener {
	private Map<String, Component> componentsRefMap;
	private SaisiesSwing context;
	
	public ValiderAction(Map<String, Component> componentsRefMap, SaisiesSwing context) {
		this.componentsRefMap = componentsRefMap;
		this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (this.isFormValid()) {
			context.save();
		}
	}

	private boolean isFormValid() {
		boolean isValid = true;
		JTextField abscisseField = (JTextField) this.componentsRefMap.get(SaisiesSwing.ABSCISSE_INPUT_TEXT_ID);
		String abscisseString = abscisseField.getText();
		try {
			Integer.parseInt(abscisseString);
		} catch (NumberFormatException e) {
			this.displayError(abscisseField);
			isValid = false;
		}
		
		JTextField ordonneeField = (JTextField) this.componentsRefMap.get(SaisiesSwing.ORDONNEE_INPUT_TEXT_ID);
		String ordonneeString = ordonneeField.getText();
		try {
			Integer.parseInt(ordonneeString);
		} catch (NumberFormatException e) {
			this.displayError(ordonneeField);
			isValid = false;
		}
		
		JTextField valeurField = (JTextField) this.componentsRefMap.get(SaisiesSwing.VALEUR_INPUT_TEXT_ID);
		String valeurString = valeurField.getText();
		try {
			Double.parseDouble(valeurString);
		} catch (NumberFormatException e) {
			this.displayError(valeurField);
			isValid = false;
		}

		return isValid;
	}
	
	private void displayError(JTextField erroredField) {
		erroredField.setBackground(Color.RED);
	}
}
