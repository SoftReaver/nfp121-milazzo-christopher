package fr.n7.cnam.nfp121.pr01.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JTextField;

import fr.n7.cnam.nfp121.pr01.SaisiesSwing;

public class EffacerAction implements ActionListener {
	private Map<String, Component> componentsRefMap;
	
	public EffacerAction(Map<String, Component> componentsRefMap) {
		this.componentsRefMap = componentsRefMap;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		((JTextField) this.componentsRefMap.get(SaisiesSwing.ABSCISSE_INPUT_TEXT_ID)).setText(null);
		((JTextField) this.componentsRefMap.get(SaisiesSwing.ORDONNEE_INPUT_TEXT_ID)).setText(null);
		((JTextField) this.componentsRefMap.get(SaisiesSwing.VALEUR_INPUT_TEXT_ID)).setText(null);
	}

}
