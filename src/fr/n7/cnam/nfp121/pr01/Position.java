package fr.n7.cnam.nfp121.pr01;

/** Définir une position.  */
public class Position {
	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override public String toString() {
		return super.toString() + "(" + x + "," + y + ")";
	}
	
	@Override
	public boolean equals(Object obj) {
		Position other = (Position) obj;
		return (this.x == other.x && this.y == other.y);
	}
}
