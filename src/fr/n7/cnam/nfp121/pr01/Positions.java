package fr.n7.cnam.nfp121.pr01;

import java.util.*;

/**
  * Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Positions extends PositionsAbstrait {
	private ArrayList<Position> positionsList;
	
	public Positions() {
		this.positionsList = new ArrayList<Position>();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.positionsList.add(position);
		super.traiter(position, valeur);
	}

	@Override
	public int nombre() {
		return positionsList.size();
	}

	@Override
	public Position position(int indice) {
		return positionsList.get(indice);
	}

	@Override
	public int frequence(Position position) {
		return Collections.frequency(this.positionsList, position);
	}

}
