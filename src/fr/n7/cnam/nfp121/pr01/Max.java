package fr.n7.cnam.nfp121.pr01;

/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {
	private Double max;
	
	public Max() {
		this.max = null;
	}
	
	public double getMax() {
		return this.max.doubleValue();
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": max = " + this.max);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if (max == null || valeur > max) {
			max = valeur;
		}
		super.traiter(position, valeur);
	}
}
