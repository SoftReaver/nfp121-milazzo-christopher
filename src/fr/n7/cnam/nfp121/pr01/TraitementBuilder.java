package fr.n7.cnam.nfp121.pr01;

import java.lang.reflect.*;
import java.util.*;

/**
  * TraitementBuilder 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class TraitementBuilder {
	
	private FabriqueTraitementConcrete fabriqueTraitementConcrete;
	private Map<String, Class<?>> primitiveTypeMap;
	
	public TraitementBuilder() {
		this.fabriqueTraitementConcrete = new FabriqueTraitementConcrete();
		this.primitiveTypeMap = new HashMap<String, Class<?>>();
		this.primitiveTypeMap.put("int", int.class);
		this.primitiveTypeMap.put("short", short.class);
		this.primitiveTypeMap.put("long", long.class);
		this.primitiveTypeMap.put("float", float.class);
		this.primitiveTypeMap.put("double", double.class);
	}
	
	/** Retourne un objet de type Class correspondant au nom en paramètre.
	 * Exemples :
	 *   - int donne int.class
	 *   - Normaliseur donne Normaliseur.class
	 */
	Class<?> analyserType(String nomType) throws ClassNotFoundException {
		Class<?> clazz = this.primitiveTypeMap.get(nomType);
		
		if (clazz == null) {
			clazz = Class.forName(nomType);
		}
		return clazz;
	}

	/** Crée l'objet java qui correspond au type formel en exploitant le "mot" suviant du scanner.
	 * Exemple : si formel est int.class, le mot suivant doit être un entier et le résulat est l'entier correspondant.
	 * Ici, on peut se limiter aux types utlisés dans le projet : int, double et String.
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) {
		String argToCast = in.next();
		Object argToReturn;
		if (formel == int.class) {
			argToReturn = Integer.parseInt(argToCast);
		} else if (formel == short.class) {
			argToReturn = Short.parseShort(argToCast);
		} else if (formel == long.class) {
			argToReturn = Long.parseLong(argToCast);
		} else if (formel == float.class) {
			argToReturn = Float.parseFloat(argToCast);
		} else if (formel == double.class) {
			argToReturn = Double.parseDouble(argToCast);
		} else {
			argToReturn = formel.cast(argToCast);
		}
		
		return argToReturn;
	}

	/** Définition de la signature, les paramètres formels, mais aussi les paramètres formels.  */
	static class Signature {
		Class<?>[] formels;
		Object[] effectifs;

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	/** Analyser une signature pour retrouver les paramètres formels et les paramètres effectifs.
	 * Exemple « 3 double 0.0 String xyz int -5 » donne
	 *   - [double.class, String.class, int.class] pour les paramètres effectifs et
	 *   - [0.0, "xyz", -5] pour les paramètres formels.
	 */
	Signature analyserSignature(Scanner in) throws ClassNotFoundException {
		int nbParams = in.nextInt();
		Class<?>[] formels = new Class<?>[nbParams];
		Object[] effectifs = new Object[nbParams];
		
		for (int i = 0; i < nbParams; i++) {
			Class<?> paramTypeClass = this.analyserType(in.next());
			Object arg = TraitementBuilder.decoderEffectif(paramTypeClass, in);
			
			formels[i] = paramTypeClass;
			effectifs[i] = arg;
		}
		Signature signatureToReturn = new Signature(formels, effectifs);
		
		return signatureToReturn;
	}


	/** Analyser la création d'un objet.
	 * Exemple : « normaliseur 2 double 0.0 double 100.0 » consiste à charger
	 * la classe Normaliseur, via la fabrique concrète trouver la méthode qui prend 2 double, et
	 * l'appeler en lui fournissant 0.0 et 100.0 comme paramètres effectifs.
	 */
	Object analyserCreation(Scanner in)
		throws ClassNotFoundException, InvocationTargetException,
						  IllegalAccessException, NoSuchMethodException,
						  InstantiationException
	{
		Object resultToReturn = null;
		Method method;
		String methodName = in.next();
		Signature methodSignature = this.analyserSignature(in);
		if (methodSignature != null) {			
			method = this.fabriqueTraitementConcrete.getClass().getDeclaredMethod(methodName, methodSignature.formels);
			resultToReturn = method.invoke(this.fabriqueTraitementConcrete, methodSignature.effectifs);
		} else {
			method = this.fabriqueTraitementConcrete.getClass().getDeclaredMethod(methodName);
			resultToReturn = method.invoke(this.fabriqueTraitementConcrete);
		}
		return resultToReturn;
	}


	/** Analyser un traitement.
	 * Exemples :
	 *   - « Somme 0 0 »
	 *   - « SupprimerPlusGrand 1 double 99.99 0 »
	 *   - « Somme 0 1 Max 0 0 »
	 *   - « Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 0 »
	 *   - « Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 1 Positions 0 0 »
	 * @param in le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 * @throws InstantiationException 
	 * @throws InvocationTargetException 
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env)
		throws ClassNotFoundException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException
	{
		// Find the ID
		String treatmentId = null;
		if (in.hasNext("id")) {
			in.next();
			treatmentId = in.next();
		}
		
		// Get the fisrt work
		Traitement traitementPrincipal = (Traitement) this.analyserCreation(in);
		
		// Get all chained works to attach to the main one
		int nbUnderWork = in.nextInt();
		for (int i = 0; i < nbUnderWork; i++) {
			traitementPrincipal.ajouterSuivants(this.analyserTraitement(in, env));
		}
		
		if (treatmentId != null && env != null) {
			env.put(treatmentId, traitementPrincipal);
		}
		return traitementPrincipal;
	}


	/** Analyser un traitement.
	 * @param in le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env)
	{
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, "
					+ "voir la cause ci-dessous", e);
		}
	}

}
